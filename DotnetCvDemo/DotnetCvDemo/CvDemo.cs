﻿using Emgu.CV;
using Emgu.CV.OCR;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;

namespace DotnetCvDemo
{
    public static class CvDemo
    {
        public static void ChangeToGrayScale()
        {
            try
            {
                string inputFname = @"D:\projects\GitLab\dotnet_cv\Files\ktp1.jpg";
                string inputFRes = @$"D:\projects\GitLab\dotnet_cv\Files\ktp-{DateTime.Now.Ticks}.jpg";
                
                using(Image<Bgr, byte> rgbImage = new Image<Bgr, byte>(inputFname))
                {
                    using(Image<Gray, byte> imgGrayScale = rgbImage.Convert<Gray, byte>())
                    {
                        Image<Gray, byte> ret = imgGrayScale;
                        var image = imgGrayScale.InRange(new Gray(127), new Gray(255));
                        var mat = imgGrayScale.Mat;
                        mat.SetTo(new MCvScalar(255, 0), image);
                        mat.CopyTo(ret);
                        ret.Save(inputFRes);

                        using (var ocr = new Tesseract(@"D:\projects\GitLab\dotnet_cv\DotnetCvDemo\DotnetCvDemo\tessdata\", "ind", OcrEngineMode.Default))
                        {
                            ocr.SetImage(ret);
                            string text = ocr.GetUTF8Text().TrimEnd();
                            Console.WriteLine(text);
                        }
                    }
                }




                
               
                //using(var engine = new Tesseract.TesseractEngine(@"D:\projects\GitLab\dotnet_cv\DotnetCvDemo\DotnetCvDemo\tessdata", "eng", EngineMode.Default))
                //{
                //   using(Page page = engine.Process(Pix.LoadFromMemory(ret.Bytes), PageSegMode.Auto))
                //   {
                //        Console.WriteLine(page.GetText());
                //   }
                //}
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
